package pages;

import java.io.IOException;

import BaseClasses.BasePage;

public class CreateProcessWithTerminatePage extends BasePage{

	public void importCrTerminateWFD() throws IOException, InterruptedException
	{
		Process process1 = Runtime.getRuntime().exec("C:\\Users\\hp\\Desktop\\Scripts\\CrProcessWithTerminateEvent.exe");
		process1.waitFor();
		System.out.println("Import WFD file with using autoit");
	}

}
