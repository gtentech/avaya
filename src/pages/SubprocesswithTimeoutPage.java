package pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import BaseClasses.BasePage;

public class SubprocesswithTimeoutPage extends BasePage{
	
	private By clickokbtn=By.xpath("//*[@id='createInstanceButton']");
	private By createprocessWFDlocator=By.xpath("//*[@id='1506497869863']/tbody/tr/td/div/text[2]");
	private By getWFDnamelocator=By.xpath("//*[@id='InstanceInfo']/tbody/tr[1]/td");
	private By workflowlocator=By.id("workflowLi");
	private By undeploylocator=By.id("undeploywfdId");
	private By undeploywindowlocator=By.xpath("//*[@id='undeployWfdModal']/div/div/div[1]/h2");
	private By okbtnlocator=By.id("confirmUndeployWfd");
	private By confirmmsglocator=By.id("successContent");
	
	

	public void importCTimeoutWFD(String WFD_Name) throws IOException, InterruptedException
	{
		Process process1 = Runtime.getRuntime().exec(WFD_Name);
		process1.waitFor();
		System.out.println("Import WFD file with using autoit");
	}

	public void clickOkbutton(WebDriver driver)
	{
		waitForElementVisibility(driver, clickokbtn);
		driver.findElement(clickokbtn).click();
		System.out.println("User is able to click on ok buttton on instance popup");
	}
	public void clickcreateWFD(WebDriver driver, SoftAssert softAssert) throws InterruptedException
	{
		Thread.sleep(8000);
		waitForElementVisibility(driver, createprocessWFDlocator);
		driver.findElement(createprocessWFDlocator).click();
		System.out.println("Click on created WFD");
		softAssert.assertEquals("Timeout_event_task", driver.findElement(getWFDnamelocator));
	}
	public void switchW(WebDriver driver)
	{
		driver.switchTo().defaultContent();
		System.out.println("user is redirected to the default window");
		Object[] allHAndlers= driver.getWindowHandles().toArray();
		System.out.println(allHAndlers.length);  
		driver.switchTo().window((String)allHAndlers[2]);
		System.out.println("user is able to switch previous window.......");
		driver.navigate().refresh();
	}
	public void workflowstab(WebDriver driver)
	{
		driver.navigate().refresh();
		waitForElementVisibility(driver, workflowlocator);
		System.out.println("wait for workflow");
		driver.findElement(workflowlocator).click();
		System.out.println("Click on workflow tab");
	}
	
	public void clickundeploywrf(WebDriver driver, SoftAssert softAssert)
	{
		waitForElementVisibility(driver, undeploylocator);
		driver.findElement(undeploylocator).click();
		System.out.println("click on undeploye btn");
		waitForElementVisibility(driver, undeploywindowlocator);
		softAssert.assertEquals("Undeploy workflow(s)", driver.findElement(undeploywindowlocator));
		System.out.println("verify undeploy window name");
		waitForElementVisibility(driver, okbtnlocator);
		driver.findElement(okbtnlocator).click();
		System.out.println("click on ok btn on undeploy window");
		waitForElementVisibility(driver, confirmmsglocator);
		softAssert.assertEquals("Successfully undeployed 1 workflow(s).", driver.findElement(confirmmsglocator));
		System.out.println("verify confirmation message");
	}
}
