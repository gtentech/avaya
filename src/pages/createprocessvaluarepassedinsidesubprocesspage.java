package pages;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import BaseClasses.BasePage;
import BaseClasses.Driver;

public class createprocessvaluarepassedinsidesubprocesspage extends BasePage{

	private By sendsms=By.xpath("//input[@name='root[SenderMsg]']");
	private By smsbody=By.xpath("//input[@name='root[MsgBody]']");
	private By receivesms=By.xpath("//input[@name='root[ReciverMsg]']");
	private By parrenttask=By.xpath("//*[@id='1506494884181']/tbody/tr/td/div/text[2]");
	private By childtask=By.xpath("//*[@id='1506494884181']/tbody/tr/td/div/text[2]");
	


	public void importfwfd() throws IOException, InterruptedException
	{
		Process process1 = Runtime.getRuntime().exec("C:\\Users\\hp\\Desktop\\Scripts\\createprocessinline.exe");
		process1.waitFor();
		System.out.println("Import WFD file with using autoit");
	}

	public void smsSend(WebDriver driver, String smstotext)
	{
		waitForElementVisibility(driver, sendsms);
		driver.findElement(sendsms).sendKeys(smstotext);
		System.out.println("Enter to sms number");
	}

	public void smsFrom(WebDriver driver,String smsFromtext)
	{
		waitForElementVisibility(driver, receivesms);
		driver.findElement(receivesms).sendKeys(smsFromtext);
		System.out.println("Enter from sms nuber");
	}

	public void smsbody(WebDriver driver, String msgbodytext)
	{
		waitForElementVisibility(driver, smsbody);
		driver.findElement(smsbody).sendKeys(msgbodytext);
		System.out.println("Enter sms body text");
	}
	public void clickparrent_task(WebDriver driver, SoftAssert softAssert) throws InterruptedException
	{
		Thread.sleep(6000);
		waitForElementVisibility(driver, parrenttask);
		driver.findElement(parrenttask).click();
		System.out.println("Click on parrent completed task");
		softAssert.assertEquals("Sendsms_Task", driver.findElement(childtask));
	}
}
