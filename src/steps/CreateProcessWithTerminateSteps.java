package steps;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import BaseClasses.BasePage;
import pages.CreateProcessWithTerminatePage;
import pages.NotificationPage;
import pages.OpenEDandConsolePage;
import pages.SubprocesswithTimeoutPage;
import pages.createprocessvaluarepassedinsidesubprocesspage;

public class CreateProcessWithTerminateSteps extends BasePage{

	
	NotificationPage notificationtask=new NotificationPage();
	OpenEDandConsolePage OpenEDConsolePage = new OpenEDandConsolePage();
	createprocessvaluarepassedinsidesubprocesspage createprocess=new createprocessvaluarepassedinsidesubprocesspage();
	SubprocesswithTimeoutPage subprocesstimeout=new SubprocesswithTimeoutPage();
	CreateProcessWithTerminatePage creatprterminate=new CreateProcessWithTerminatePage();
	
	public void createprocessWithTerminate(WebDriver driver, SoftAssert softAssert) throws IOException, InterruptedException
	{
		OpenEDConsolePage.clickAvayalink(driver,softAssert);
		OpenEDConsolePage.selectnodedropdownoptions(driver, softAssert);
		notificationtask.importworkflowmenuicon(driver, softAssert);
		notificationtask.uploadfile(driver, softAssert);
		Thread.sleep(6000);
		creatprterminate.importCrTerminateWFD();
		notificationtask.clickimportbtn(driver, softAssert);
	}
	public void clickOkInstancebtn(WebDriver driver)
	{
		subprocesstimeout.clickOkbutton(driver);
	}
	public void openparrenttask(WebDriver driver, SoftAssert softAssert) throws InterruptedException
	{
		subprocesstimeout.clickcreateWFD(driver, softAssert);
	}
}
