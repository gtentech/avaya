package steps;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import BaseClasses.BasePage;
import BaseClasses.Driver;
import jxl.read.biff.BiffException;
import pages.NotificationPage;
import pages.OpenEDandConsolePage;
import pages.SubprocesswithTimeoutPage;
import pages.createprocessvaluarepassedinsidesubprocesspage;

public class SubprocesswithTimeoutSteps extends BasePage{
	NotificationPage notificationtask=new NotificationPage();
	OpenEDandConsolePage OpenEDConsolePage = new OpenEDandConsolePage();
	createprocessvaluarepassedinsidesubprocesspage createprocess=new createprocessvaluarepassedinsidesubprocesspage();
	SubprocesswithTimeoutPage subprocesstimeout=new SubprocesswithTimeoutPage();
	
	public void openEDandAdminpage(WebDriver driver, SoftAssert softAssert) throws InterruptedException, IOException
	{
		OpenEDConsolePage.clickAvayalink(driver,softAssert);
		OpenEDConsolePage.selectnodedropdownoptions(driver, softAssert);
	}
	public void createprocesswithinsidevalue(WebDriver driver, SoftAssert softAssert, String WFD_Name) throws IOException, InterruptedException, BiffException
	{
		/*OpenEDConsolePage.clickAvayalink(driver,softAssert);
		OpenEDConsolePage.selectnodedropdownoptions(driver, softAssert);*/
		notificationtask.importworkflowmenuicon(driver, softAssert);
		notificationtask.uploadfile(driver, softAssert);
		Thread.sleep(8000);
		subprocesstimeout.importCTimeoutWFD(WFD_Name);
		notificationtask.clickimportbtn(driver, softAssert);
	}
	public void clickOkInstancebtn(WebDriver driver)
	{
		subprocesstimeout.clickOkbutton(driver);
	}
	public void openparrenttask(WebDriver driver, SoftAssert softAssert) throws InterruptedException
	{
		subprocesstimeout.clickcreateWFD(driver, softAssert);
	}
	public void switchingWindowpre(WebDriver driver)
	{
		subprocesstimeout.switchW(driver);
	}
	
	public void clickworkflowtab(WebDriver driver, SoftAssert softAssert,String randomname) throws InterruptedException
	{
		subprocesstimeout.workflowstab(driver);
		notificationtask.clickrefreshbtn(driver);
		OpenEDConsolePage.searchcreatedinstance(driver, softAssert, randomname);
		OpenEDConsolePage.instancecheckbox(driver, softAssert);
		subprocesstimeout.clickundeploywrf(driver, softAssert);
	}
}
