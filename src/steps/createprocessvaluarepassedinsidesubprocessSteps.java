package steps;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import BaseClasses.BasePage;
import pages.NotificationPage;
import pages.OpenEDandConsolePage;
import pages.createprocessvaluarepassedinsidesubprocesspage;

public class createprocessvaluarepassedinsidesubprocessSteps extends BasePage {
	NotificationPage notificationtask=new NotificationPage();
	OpenEDandConsolePage OpenEDConsolePage = new OpenEDandConsolePage();
	createprocessvaluarepassedinsidesubprocesspage createprocess=new createprocessvaluarepassedinsidesubprocesspage();

	public void createprocesswithinsidevalue(WebDriver driver, SoftAssert softAssert) throws IOException, InterruptedException
	{
		OpenEDConsolePage.clickAvayalink(driver,softAssert);
		OpenEDConsolePage.selectnodedropdownoptions(driver, softAssert);
		notificationtask.importworkflowmenuicon(driver, softAssert);
		notificationtask.uploadfile(driver, softAssert);
		Thread.sleep(6000);
		createprocess.importfwfd();
		notificationtask.clickimportbtn(driver, softAssert);
	}
	public void entermsgdetails(WebDriver driver, SoftAssert softAssert, String smstotext, String smsFromtext, String msgbodytext)
	{
		createprocess.smsSend(driver, smstotext);
		createprocess.smsFrom(driver, smsFromtext);
		createprocess.smsbody(driver, msgbodytext);
		notificationtask.createinstanceokbtn(driver, softAssert);
	}
	public void openparrent(WebDriver driver, SoftAssert softAssert) throws InterruptedException
	{
		createprocess.clickparrent_task(driver,softAssert);
	}
}
