package tests;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import BaseClasses.BasePage;
import BaseClasses.Driver;
import steps.CreateProcessWithTerminateSteps;
import steps.LoginSteps;
import steps.NotificationSteps;
import steps.SubprocesswithTimeoutSteps;
import steps.createprocessvaluarepassedinsidesubprocessSteps;
import utilities.ExtentFactory;

public class CreateProcessWithTerminateTests extends BasePage{

	String ranNumberrandom;
	createprocessvaluarepassedinsidesubprocessSteps createprocesstest=new createprocessvaluarepassedinsidesubprocessSteps();
	NotificationSteps notificationstepstask=new NotificationSteps();
	SubprocesswithTimeoutSteps subprocesstimeoutStp=new SubprocesswithTimeoutSteps();
	CreateProcessWithTerminateSteps createprocessterminatestp=new CreateProcessWithTerminateSteps();

	Driver driverObj=new Driver();
	WebDriver driver=null;
	LoginSteps loginSteps=new LoginSteps();
	SoftAssert softAssert=new SoftAssert();
	ExtentFactory extentlog=new ExtentFactory();

	@SuppressWarnings("static-access")
	@BeforeTest
	public void start() throws IOException{
		driver=driverObj.createDriver();
		driver.get(driverObj.getUrl());
	}
	@Test(priority=1)
	public void loginWithValidUserDetails(){
		loginSteps.login(driver,softAssert,driverObj.getUsername(),driverObj.getPassword());
		//loginSteps.logout(driver,softAssert);
		softAssert.assertAll();
	}
	@Test(priority=2)
	public void notificationtaskcompleted() throws IOException, InterruptedException
	{
		createprocessterminatestp.createprocessWithTerminate(driver, softAssert);
		ranNumberrandom=driverObj.getrandomname3()+ranNumber(2);
		notificationstepstask.savenotificationWFD(driver, softAssert, ranNumberrandom);
		notificationstepstask.Openadminconsole(driver, softAssert, ranNumberrandom);
		subprocesstimeoutStp.clickOkInstancebtn(driver);
		//createprocesstest.entermsgdetails(driver, softAssert, driverObj.getsmstotext(),driverObj.getsmsFromtext(),driverObj.getmsgbodytext());
		notificationstepstask.createinstance(driver, softAssert, ranNumberrandom);
		subprocesstimeoutStp.openparrenttask(driver, softAssert);
		notificationstepstask.refreshbtn(driver, softAssert);
	}

	@Test(priority=0)
	public void loginWithInvalidUserDeatils(){
		loginSteps.invalidLogin(driver,softAssert,driverObj.getInvalidUsername(),driverObj.getPassword());
	}

	@AfterTest
	public void tearDown(){
		driver.quit();
		driver=null;
	}

}
