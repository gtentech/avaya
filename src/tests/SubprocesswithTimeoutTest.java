package tests;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import BaseClasses.BasePage;
import BaseClasses.Driver;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import steps.LoginSteps;
import steps.NotificationSteps;
import steps.SubprocesswithTimeoutSteps;
import steps.createprocessvaluarepassedinsidesubprocessSteps;
import utilities.ExtentFactory;

public class SubprocesswithTimeoutTest extends BasePage {
	String ranNumberrandom;
	createprocessvaluarepassedinsidesubprocessSteps createprocesstest=new createprocessvaluarepassedinsidesubprocessSteps();
	NotificationSteps notificationstepstask=new NotificationSteps();
	SubprocesswithTimeoutSteps subprocesstimeoutStp=new SubprocesswithTimeoutSteps();
	Driver driverObj=new Driver();
	WebDriver driver=null;
	LoginSteps loginSteps=new LoginSteps();
	SoftAssert softAssert=new SoftAssert();
	ExtentFactory extentlog=new ExtentFactory();


	public int rowCount;
	@DataProvider
	public Object[][] prepareData() throws BiffException, IOException {

		String filePath = System.getProperty("user.dir") + "/Readexefile/data.xls";

		File inputWorkbook = new File(filePath);

		Workbook w = Workbook.getWorkbook(inputWorkbook);

		// Get the first sheet
		Sheet sheet = w.getSheet(0);
		// Loop over first 10 column and lines
		int rowCount = sheet.getRows();
		int coulumnCount = sheet.getColumns();

		Object data[][] = new Object[rowCount - 1][coulumnCount];

		//read data from excel(loop will start from 1 bcoz we dont want to 1st row which specify column name i.e. user and pass)
		for (int i = 1; i < rowCount; i++) {
			Cell cell = sheet.getCell(0, i);
			String value = cell.getContents();

			//Store data in excel( we have use i-1 to store data at oth position i.e. 0,0 in memory)
			data[i - 1][0] = value;

			/*cell = sheet.getCell(1, i);
			value = cell.getContents();
			data[i - 1][1] = value;*/
		}
		w.close();

		return data;
	}


	@SuppressWarnings("static-access")
	@BeforeTest
	public void start() throws IOException{
		driver=driverObj.createDriver();
		driver.get(driverObj.getUrl());
	}
	@Test(priority=1)
	public void loginWithValidUserDetails(){
		loginSteps.login(driver,softAssert,driverObj.getUsername(),driverObj.getPassword());
		//loginSteps.logout(driver,softAssert);
		softAssert.assertAll();
	}
	@Test(priority=2)
	public void edandadmin() throws InterruptedException, IOException
	{
		subprocesstimeoutStp.openEDandAdminpage(driver, softAssert);
	}
  
	@Test(priority=3,dataProvider = "prepareData")
	public void notificationtaskcompleted(String WFD_Name) throws IOException, InterruptedException, BiffException
	{
		subprocesstimeoutStp.createprocesswithinsidevalue(driver, softAssert, WFD_Name);
		ranNumberrandom=driverObj.getrandomname2()+ranNumber(5);
		notificationstepstask.savenotificationWFD(driver, softAssert, ranNumberrandom);
		notificationstepstask.Openadminconsole(driver, softAssert, ranNumberrandom);
		subprocesstimeoutStp.clickOkInstancebtn(driver);
		//createprocesstest.entermsgdetails(driver, softAssert, driverObj.getsmstotext(),driverObj.getsmsFromtext(),driverObj.getmsgbodytext());
		notificationstepstask.createinstance(driver, softAssert, ranNumberrandom);
		//subprocesstimeoutStp.openparrenttask(driver, softAssert);
		notificationstepstask.refreshbtn(driver, softAssert);
		Thread.sleep(5000);
		subprocesstimeoutStp.clickworkflowtab(driver, softAssert, ranNumberrandom);
		subprocesstimeoutStp.switchingWindowpre(driver);
		
	}

	/*@Test(priority=0)
	public void loginWithInvalidUserDeatils(){
		loginSteps.invalidLogin(driver,softAssert,driverObj.getInvalidUsername(),driverObj.getPassword());
	}*/

	@AfterTest
	public void tearDown(){
		driver.quit();
		driver=null;
	}

}
