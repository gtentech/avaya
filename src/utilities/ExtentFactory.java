package utilities;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentFactory {

	public static ExtentReports getInstance(){
		ExtentReports extent=new ExtentReports (System.getProperty("user.dir") +"/test-output/ExtentReport.html", true);
		extent
		  .addSystemInfo("Selenium"," QA")
		  .addSystemInfo("UserName", "kushagra")
		  .addSystemInfo("Plateform","OS");
		return extent;
		}
}
